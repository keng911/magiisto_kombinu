# Magiisto, kombinu!

This game was made for the [2nd Bevy Game Jam](https://itch.io/jam/bevy-jam-2) see it's [Itch.io Site](https://akito98.itch.io/magiisto-kombinu) for more info about the game.

The code hasn't been changed since the jam, you will still find the bugs and hacky code that tend to happen during a ten day coding event.
Maybe I will come back to this project some time in the future and refactor it and add some features I had initially planned but probably not.
You may still look at the code if something is usefull for you.

# Build

## Web

I used [Trunk](https://trunkrs.dev/#install) to generate the web build.
After installing it, just run `trunk build`.
Then copy the assets folder into the new dist folder
I also had to change the paths in index.html from `/combine-` to `./combine-`

## Native

`cargo build --release`
copy asset and executable in the same folder

# External Assets

The font FiraSans is available from [here](https://fonts.google.com/specimen/Fira+Sans)
The only bevy plugin I used is [iyes_loopless](https://github.com/IyesGames/iyes_loopless)

# License

Just like bevy itself, this project is dual-licensed under either:

	MIT License (LICENSE-MIT or http://opensource.org/licenses/MIT)
	Apache License, Version 2.0 (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)

whatever you prefer.
