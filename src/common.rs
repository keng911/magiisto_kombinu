use bevy::{
	prelude::*,
	render::camera::RenderTarget
};

use rand::prelude::*;

use crate::player::{
	CAMERA_MAX_DISTANCE_X,
	CAMERA_MAX_DISTANCE_Y,
	Player
};

///////////////////////////////////////////////////////////////////
//                          C O N S T S                          //
///////////////////////////////////////////////////////////////////

pub const TIME_STEP: f32 = 1.0 / 60.0;
// 360 deg in rad
pub const ROTATION_SPEED: f32 = 6.283185 * TIME_STEP;
pub const WINDOW_WIDTH: f32 = 1200.0;
pub const WINDOW_HEIGHT: f32 = 800.0;

///////////////////////////////////////////////////////////////////
//                         S T R U C T S                         //
///////////////////////////////////////////////////////////////////

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum GameState {
	MainMenu,
	Playing,
	Menu,
	GameOver
}

#[derive(Component)]
pub struct Health {
	pub max_health: f32,
	pub health: f32
}

#[derive(Component)]
pub struct MovementAnimation {
	pub moving: bool,
	pub timer: Timer
}

pub struct DamagePlayerEvent {
	pub damage: f32
}

pub struct DamageEnemyEvent {
	pub entity: Entity,
	pub damage: f32
}

pub struct DamageSnowmanEvent {
	pub damage: f32
}

#[derive(Component)]
pub struct Collision;

#[derive(Component)]
pub struct Dead {
	pub despawn_timer: Timer
}

#[derive(Component)]
pub struct Foliage;
#[derive(Resource)]
pub struct Points {
	pub total_points: i32,
	pub points: i32
}

///////////////////////////////////////////////////////////////////
//                         S Y S T E M S                         //
///////////////////////////////////////////////////////////////////

pub fn setup_map(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
	query_camera: Query<With<Camera>>,
) {
	if !query_camera.is_empty() {
		// Setup already done
		return;
	}

	commands.spawn(Camera2dBundle::default());

	let mut rng = thread_rng();

	let texture_handle = asset_server.load("foliage.png");
	// let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 4, 1);
	let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 4, 1, None, None);
	let texture_atlas_handle = texture_atlases.add(texture_atlas);

	for _ in 0..300 {
		commands.spawn(SpriteSheetBundle {
			texture_atlas: texture_atlas_handle.clone(),
			sprite: TextureAtlasSprite {
				index: rng.gen_range(0..4),
				..default()
			},
			transform: Transform::from_translation(Vec3::new(rng.gen_range(-1000.0..1000.0), rng.gen_range(-1000.0..1000.0), 0.0)),
			..default()
		})
		.insert(Foliage);
	}

	// Preload assets
	let _: Handle<Image> = asset_server.load("enemy.png");
	let _: Handle<Image> = asset_server.load("player.png");

	let _: Handle<Image> = asset_server.load("spells/bomb_fragment.png");
	let _: Handle<Image> = asset_server.load("spells/bomb.png");
	let _: Handle<Image> = asset_server.load("spells/energy_bolt.png");
	let _: Handle<Image> = asset_server.load("spells/fireball.png");
	let _: Handle<Image> = asset_server.load("spells/flame.png");
	let _: Handle<Image> = asset_server.load("spells/ice_speer.png");
	let _: Handle<Image> = asset_server.load("spells/lightning.png");
	let _: Handle<Image> = asset_server.load("spells/plasma.png");
	let _: Handle<Image> = asset_server.load("spells/snowball_fragment.png");
	let _: Handle<Image> = asset_server.load("spells/snowball.png");
	let _: Handle<Image> = asset_server.load("spells/snowman.png");

	let _: Handle<Image> = asset_server.load("icons/bomb_inactive.png");
	let _: Handle<Image> = asset_server.load("icons/bomb.png");
	let _: Handle<Image> = asset_server.load("icons/empty.png");
	let _: Handle<Image> = asset_server.load("icons/energy_bolt.png");
	let _: Handle<Image> = asset_server.load("icons/energy.png");
	let _: Handle<Image> = asset_server.load("icons/fireball.png");
	let _: Handle<Image> = asset_server.load("icons/fire.png");
	let _: Handle<Image> = asset_server.load("icons/flamethrower_inactive.png");
	let _: Handle<Image> = asset_server.load("icons/flamethrower.png");
	let _: Handle<Image> = asset_server.load("icons/ice.png");
	let _: Handle<Image> = asset_server.load("icons/ice_speer.png");
	let _: Handle<Image> = asset_server.load("icons/key_e.png");
	let _: Handle<Image> = asset_server.load("icons/lightning_inactive.png");
	let _: Handle<Image> = asset_server.load("icons/lightning.png");
	let _: Handle<Image> = asset_server.load("icons/mouse_left.png");
	let _: Handle<Image> = asset_server.load("icons/mouse_right.png");
	let _: Handle<Image> = asset_server.load("icons/plasma_inactive.png");
	let _: Handle<Image> = asset_server.load("icons/plasma.png");
	let _: Handle<Image> = asset_server.load("icons/snowball_inactive.png");
	let _: Handle<Image> = asset_server.load("icons/snowball.png");
	let _: Handle<Image> = asset_server.load("icons/snowman_inactive.png");
	let _: Handle<Image> = asset_server.load("icons/snowman.png");
}

pub fn check_collisions(
	mut query: Query<(&mut Transform, Option<&Player>), With<Collision>>,
	mut query_camera: Query<&mut Transform, (With<Camera>, Without<Collision>)>
) {
	let mut camera_transform = query_camera.single_mut();

	let mut combinations = query.iter_combinations_mut();
	while let Some([(mut transform1, player1), (mut transform2, player2)]) = combinations.fetch_next() {
		if transform1.translation.distance(transform2.translation) < 45.0 {
			let direction1 = (transform1.rotation * Vec3::Y).truncate();
			let direction2 = (transform2.rotation * Vec3::Y).truncate();
			let delta = (direction2 - direction1) * 2.0;

			transform1.translation.x += delta.x;
			transform1.translation.y += delta.y;

			transform2.translation.x -= delta.x;
			transform2.translation.y -= delta.y;

			// Move Camera if player is pushed around
			// Enemies shouldn't push the player around because they stop moving and attack instead
			// but I can image that it can happen if they are in turn pushed from behind
			if player1.is_some() {
				if (transform1.translation.x - camera_transform.translation.x).abs() > CAMERA_MAX_DISTANCE_X {
					camera_transform.translation.x += delta.x;
				}
				if (transform1.translation.y - camera_transform.translation.y).abs() > CAMERA_MAX_DISTANCE_Y {
					camera_transform.translation.y += delta.y;
				}
			} else if player2.is_some() {
				if (transform2.translation.x - camera_transform.translation.x).abs() > CAMERA_MAX_DISTANCE_X {
					camera_transform.translation.x -= delta.x;
				}
				if (transform2.translation.y - camera_transform.translation.y).abs() > CAMERA_MAX_DISTANCE_Y {
					camera_transform.translation.y -= delta.y;
				}
			}
		}
	}
}

pub fn animate_movement(
	mut query: Query<(&mut MovementAnimation, &mut TextureAtlasSprite)>,
	time: Res<Time>
) {
	for (mut animation, mut texture) in &mut query {
		if animation.moving {
			animation.timer.tick(time.delta());

			if animation.timer.finished() {
				texture.index += 1;
				if texture.index > 8 {
					texture.index = 1;
				}
			}
		} else {
			texture.index = 0;
		}
	}
}

///////////////////////////////////////////////////////////////////
//                       F U N C T I O N S                       //
///////////////////////////////////////////////////////////////////

// Adopted from https://bevy-cheatbook.github.io/cookbook/cursor2world.html
pub fn get_world_pos_from_mouse_pos(
	wnds: &Windows,
	camera: &Camera,
	camera_transform: &GlobalTransform
) -> Option<Vec2> {
	let wnd = if let RenderTarget::Window(id) = camera.target {
		wnds.get(id).unwrap()
	} else {
		wnds.get_primary().unwrap()
	};

	if let Some(screen_pos) = wnd.cursor_position() {
		let window_size = Vec2::new(wnd.width() as f32, wnd.height() as f32);
		let ndc = (screen_pos / window_size) * 2.0 - Vec2::ONE;
		let ndc_to_world = camera_transform.compute_matrix() * camera.projection_matrix().inverse();
		let world_pos = ndc_to_world.project_point3(ndc.extend(-1.0));
		let world_pos: Vec2 = world_pos.truncate();

		return Some(world_pos);
	}

	return None;
}
