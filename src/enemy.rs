use bevy::{
	prelude::*,
	math::Vec3Swizzles,
	utils::Duration
};

use iyes_loopless::prelude::*;

use rand::prelude::*;

use crate::common::{
	ROTATION_SPEED,
	GameState,
	Health,
	DamageEnemyEvent,
	DamagePlayerEvent,
	DamageSnowmanEvent,
	Dead,
	Points,
	MovementAnimation,
	Collision
};

use crate::player::Player;

use crate::spells::SpellSnowman;

use crate::gui::spawn_health_bar;

///////////////////////////////////////////////////////////////////
//                          C O N S T S                          //
///////////////////////////////////////////////////////////////////

const ENEMY_SPAWN_TIME: u64 = 2;

///////////////////////////////////////////////////////////////////
//                          P L U G I N                          //
///////////////////////////////////////////////////////////////////

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
	fn build(&self, app: &mut App) {
		app
			.init_resource::<EnemySpawnTimer>()
			.init_resource::<EnemyStats>()
			.add_system(update_enemy_stats.run_in_state(GameState::Playing))
			.add_system(spawn_enemies.run_in_state(GameState::Playing))
			.add_system(despawn_enemies.run_in_state(GameState::Playing))
			.add_system(enemy_move_attack.run_in_state(GameState::Playing).label("enemy_move_attack"))
			.add_system(enemy_rotation.run_in_state(GameState::Playing))
			.add_system(enemy_receive_damage.run_in_state(GameState::Playing));
	}
}

///////////////////////////////////////////////////////////////////
//                         S T R U C T S                         //
///////////////////////////////////////////////////////////////////

#[derive(Component)]
pub struct Enemy {
	attack_cooldown: Timer
}
#[derive(Resource)]
struct EnemySpawnTimer {
	timer: Timer
}

impl FromWorld for EnemySpawnTimer {
	fn from_world(_world: &mut World) -> Self {
		EnemySpawnTimer {
			timer: Timer::new(Duration::from_secs(ENEMY_SPAWN_TIME), TimerMode::Repeating)
		}
	}
}
#[derive(Resource)]
pub struct EnemyStats {
	max_health: f32,
	speed: f32,
	attack_speed: u64,
	damage: f32,
	update_timer: Timer
}

impl FromWorld for EnemyStats {
	fn from_world(_world: &mut World) -> Self {
		EnemyStats {
			max_health: 90.0,
			speed: 120.0,
			attack_speed: 1,
			damage: 5.0,
			update_timer: Timer::new(Duration::from_secs(5), TimerMode::Repeating)
		}
	}
}
///////////////////////////////////////////////////////////////////
//                         S Y S T E M S                         //
///////////////////////////////////////////////////////////////////

fn spawn_enemies(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	time: Res<Time>,
	mut spawn_timer: ResMut<EnemySpawnTimer>,
	enemy_stats: Res<EnemyStats>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>
) {
	let mut rng = thread_rng();

	spawn_timer.timer.tick(time.delta());

	if spawn_timer.timer.just_finished() {
		let texture_handle = asset_server.load("enemy.png");
		let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(128.0, 128.0), 10, 1, None,None);
		let texture_atlas_handle = texture_atlases.add(texture_atlas);

		let side = rng.gen_range(0..4);
		let translation = match side {
			// Top
			0 => Vec3::new(rng.gen_range(-1000.0..1000.0), -1500.0, 1.0),
			// Right
			1 => Vec3::new(-1500.0, rng.gen_range(-1000.0..1000.0), 1.0),
			// Bottom
			2 => Vec3::new(rng.gen_range(-1000.0..1000.0), 1500.0, 1.0),
			// Left
			3 => Vec3::new(1500.0, rng.gen_range(-1000.0..1000.0), 1.0),
			_ => Vec3::new(0.0, 0.0, 1.0)
		};

		let entity = commands.spawn(SpriteSheetBundle {
			texture_atlas: texture_atlas_handle,
			sprite: TextureAtlasSprite {
				// Start with a random index so that they don't all walk synchronously
				index: rng.gen_range(1..8),
				..default()
			},
			transform: Transform::from_translation(translation),
			..default()
		})
		.insert(Enemy {
			attack_cooldown: Timer::new(Duration::from_secs(enemy_stats.attack_speed), TimerMode::Once)
		})
		.insert(MovementAnimation {
			moving: false,
			timer: Timer::new(Duration::from_millis(300), TimerMode::Repeating)
		})
		.insert(Health {
			max_health: enemy_stats.max_health,
			health: enemy_stats.max_health
		})
		.insert(Collision)
		.id();

		spawn_health_bar(&mut commands, entity.to_bits());
	}
}

fn update_enemy_stats(
	mut stats: ResMut<EnemyStats>,
	time: Res<Time>
) {
	stats.update_timer.tick(time.delta());

	if stats.update_timer.finished() {
		stats.max_health += 0.5;
		if stats.speed < 200.0 {
			stats.speed += 0.5;
		}
		if stats.damage < 15.0 {
			stats.damage += 0.2;
		}
	}
}

fn despawn_enemies(
	mut commands: Commands,
	time: Res<Time>,
	mut query: Query<(Entity, &mut Dead), With<Enemy>>
) {
	for (entity, mut dead) in &mut query {
		dead.despawn_timer.tick(time.delta());

		if dead.despawn_timer.finished() {
			commands.entity(entity).despawn_recursive();
		}
	}
}

pub fn enemy_move_attack(
	query_snowman: Query<&Transform, (With<SpellSnowman>, Without<Player>, Without<Enemy>)>,
	query_player: Query<&Transform, With<Player>>,
	mut query_enemy: Query<(&mut Transform, &mut MovementAnimation, &mut Enemy), (Without<Dead>, Without<Player>)>,
	time: Res<Time>,
	mut event_writer_player: EventWriter<DamagePlayerEvent>,
	mut event_writer_snowman: EventWriter<DamageSnowmanEvent>,
	enemy_stats: Res<EnemyStats>
) {
	let mut is_snowman = false;

	let target = if query_snowman.is_empty() {
		query_player.single().translation
	} else {
		is_snowman = true;
		query_snowman.single().translation
	};

	for (mut transform, mut animation, mut enemy) in &mut query_enemy {
		enemy.attack_cooldown.tick(time.delta());

		if transform.translation.distance(target) < 50.0 {
			// In attack range
			animation.moving = false;
			if enemy.attack_cooldown.finished() {
				enemy.attack_cooldown.reset();
				if is_snowman {
					event_writer_snowman.send(DamageSnowmanEvent {
						damage: enemy_stats.damage
					});
				} else {
					event_writer_player.send(DamagePlayerEvent {
						damage: enemy_stats.damage
					});
				}
			}
		} else {
			// Move
			animation.moving = true;
			let movement_direction = transform.rotation * Vec3::Y;
			let movement_distance = enemy_stats.speed * time.delta_seconds();

			let translation_delta = movement_direction * movement_distance;

			transform.translation += translation_delta;
		}
	}
}

fn enemy_rotation(
	query_snowman: Query<&Transform, (With<SpellSnowman>, Without<Player>, Without<Enemy>)>,
	query_player: Query<&Transform, With<Player>>,
	mut query: Query<&mut Transform, (With<Enemy>, Without<Dead>, Without<Player>)>,
) {
	let target = if query_snowman.is_empty() {
		query_player.single().translation
	} else {
		query_snowman.single().translation
	};

	for mut transform in &mut query {
		let forward = (transform.rotation * Vec3::Y).xy();
		let to_player = (target.xy() - transform.translation.xy()).normalize();
		let forward_dot_player = forward.dot(to_player);

		if (forward_dot_player - 1.0).abs() < f32::EPSILON {
			return;
		}

		let right = (transform.rotation * Vec3::X).xy();
		let right_dot_player = right.dot(to_player);
		let rotation_sign = -f32::copysign(1.0, right_dot_player);
		let max_angle = forward_dot_player.clamp(-1.0, 1.0).acos();
		let rotation_angle = rotation_sign * (ROTATION_SPEED.min(max_angle));

		transform.rotate_z(rotation_angle);
	}
}

fn enemy_receive_damage(
	mut commands: Commands,
	mut events: EventReader<DamageEnemyEvent>,
	mut query: Query<(&mut Health, &mut Transform, &mut TextureAtlasSprite), (With<Enemy>, Without<Dead>)>,
	mut points: ResMut<Points>
) {
	// If two projectiles kill the same enemy in one frame (tends to happen with the flamethrower because of the many projectiles)
	// then there are two events that both trigger here and give the player more points than they deserve
	// To avoid this, save a list of enemies that died this frame and ignore them if they want to die again
	let mut died_enemies: Vec<Entity> = Vec::new();

	for event in events.iter() {
		if died_enemies.contains(&event.entity) {
			continue;
		}

		if let Ok((mut health, mut transform, mut texture)) = query.get_mut(event.entity) {
			let mut rng = thread_rng();

			health.health = health.health - event.damage;

			if health.health <= 0.0 {
				died_enemies.push(event.entity);

				points.total_points += 10;
				points.points += 10;

				// Random z value to avoid flickering if two overlap
				transform.translation.z = rng.gen_range(0.1..0.9);
				texture.index = 9;

				commands.entity(event.entity)
					.insert(Dead {
						despawn_timer: Timer::new(Duration::from_secs(20), TimerMode::Once)
					})
					.remove::<MovementAnimation>()
					.remove::<Collision>();
			}
		}
	}
}

///////////////////////////////////////////////////////////////////
//                       F U N C T I O N S                       //
///////////////////////////////////////////////////////////////////

