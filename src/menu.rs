use bevy::prelude::*;

use iyes_loopless::prelude::*;

use crate::common::{
	GameState,
	Points
};

use crate::player::Player;
use crate::enemy::Enemy;
use crate::spells::Spell;
use crate::spells::SpellType;
use crate::gui::HealthBar;

///////////////////////////////////////////////////////////////////
//                          C O N S T S                          //
///////////////////////////////////////////////////////////////////

const COLOR_BACKGROUND: Color = Color::rgb(0.1, 0.1, 0.1);
const COLOR_FOREGROUND: Color = Color::rgb(0.6, 0.6, 0.6);
const COLOR_SELECTED: Color = Color::rgb(0.9, 0.7, 0.42);
const COLOR_TEXT: Color = Color::rgb(0.9, 0.9, 0.9);

///////////////////////////////////////////////////////////////////
//                          P L U G I N                          //
///////////////////////////////////////////////////////////////////

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
	fn build(&self, app: &mut App) {
		app
			.init_resource::<DiscoveredSpells>()

			.add_enter_system(GameState::MainMenu, setup_main_menu)
			.add_system(update_main_menu.run_in_state(GameState::MainMenu))

			.add_system(open_menu.run_in_state(GameState::Playing))
			.add_system(close_menu.run_in_state(GameState::Menu))

			.add_system(select_crafting_spell.run_in_state(GameState::Menu))
			.add_system(craft_spell.run_in_state(GameState::Menu))
			.add_system(update_spell_desctiption.run_in_state(GameState::Menu))
			.add_system(select_spell.run_in_state(GameState::Menu))

			.add_enter_system(GameState::GameOver, game_over)
			.add_system(update_game_over.run_in_state(GameState::GameOver));
	}
}

///////////////////////////////////////////////////////////////////
//                         S T R U C T S                         //
///////////////////////////////////////////////////////////////////
#[derive(Resource)]
struct DiscoveredSpells {
	flamethrower: bool,
	bomb: bool,
	plasma: bool,
	snowball: bool,
	snowman: bool,
	lightning: bool
}

impl FromWorld for DiscoveredSpells {
	fn from_world(_world: &mut World) -> Self {
		DiscoveredSpells {
			flamethrower: false,
			bomb: false,
			plasma: false,
			snowball: false,
			snowman: false,
			lightning: false,
		}
	}
}

#[derive(Component)]
struct MainMenuItem;

#[derive(Component)]
struct GameOverItem;

#[derive(Component)]
struct MenuItem;

#[derive(Component)]
struct SpellSlot;

#[derive(Component)]
struct CraftingCostText;

#[derive(Component)]
struct SpellDescriptionText;

#[derive(Component)]
struct SelectedSpellIcon;

#[derive(Component, Copy, Clone)]
struct CraftingRow1;

#[derive(Component, Copy, Clone)]
struct CraftingRow2;

#[derive(Component, Copy, Clone)]
struct CraftingFire;

#[derive(Component, Copy, Clone)]
struct CraftingIce;

#[derive(Component, Copy, Clone)]
struct CraftingEnergy;

#[derive(Component, Copy, Clone)]
struct CraftingResult;

#[derive(Component, Copy, Clone)]
struct SpellList;

#[derive(Component, Copy, Clone)]
struct Dummy;

///////////////////////////////////////////////////////////////////
//                         S Y S T E M S                         //
///////////////////////////////////////////////////////////////////

fn setup_main_menu(
	query: Query<Entity, With<GameOverItem>>,
	asset_server: Res<AssetServer>,
	mut commands: Commands
) {
	for entity in &query {
		commands.entity(entity).despawn_recursive();
	}

	// Background
	commands.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			justify_content: JustifyContent::Center,
			align_items: AlignItems::Center,
			..default()
		},
		background_color: COLOR_BACKGROUND.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(
			TextBundle::from_sections([
				TextSection::new(
					"Magiisto, kombinu!\n\n",
					TextStyle {
						font: asset_server.load("FiraSans-Bold.ttf"),
						font_size: 100.0,
						color: COLOR_TEXT
					}
				),
				TextSection::new(
					"Press space to start",
					TextStyle {
						font: asset_server.load("FiraSans-Bold.ttf"),
						font_size: 32.0,
						color: COLOR_TEXT
					}
				)
			])
			.with_text_alignment(TextAlignment::CENTER)
		);
	})
	.insert(MainMenuItem);
}

fn update_main_menu(
	keys: Res<Input<KeyCode>>,
	mut commands: Commands,
	query: Query<Entity, With<MainMenuItem>>
) {
	if keys.just_pressed(KeyCode::Space) {
		for entity in &query {
			commands.entity(entity).despawn_recursive();
		}
		commands.insert_resource(NextState(GameState::Playing));
	}
}

fn game_over(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	mut points: ResMut<Points>,
	discovered_spells: Res<DiscoveredSpells>,
	mut set: ParamSet<(
		Query<&mut Transform, With<Camera>>,
		Query<Entity, With<Player>>,
		Query<Entity, With<Enemy>>,
		Query<Entity, With<Spell>>,
		Query<Entity, With<HealthBar>>
	)>
) {
	let mut unlocked_spells = 3;
	if discovered_spells.flamethrower { unlocked_spells += 1; }
	if discovered_spells.bomb { unlocked_spells += 1; }
	if discovered_spells.plasma { unlocked_spells += 1; }
	if discovered_spells.snowball { unlocked_spells += 1; }
	if discovered_spells.snowman { unlocked_spells += 1; }
	if discovered_spells.lightning { unlocked_spells += 1; }

	commands.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			justify_content: JustifyContent::Center,
			align_items: AlignItems::Center,
			..default()
		},
		background_color: COLOR_BACKGROUND.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(
			TextBundle::from_sections([
				TextSection::new(
					"Game Over!\n\n",
					TextStyle {
						font: asset_server.load("FiraSans-Bold.ttf"),
						font_size: 100.0,
						color: COLOR_TEXT
					}
				),
				TextSection::new(
					format!("You got a total of {:?} points! (Current points: {:?})\nAnd discovered {:?} of 9 spells.\n\nPress space to return", points.total_points, points.points, unlocked_spells),
					TextStyle {
						font: asset_server.load("FiraSans-Bold.ttf"),
						font_size: 32.0,
						color: COLOR_TEXT
					}
				)
			])
			.with_text_alignment(TextAlignment::CENTER)
		);
	})
	.insert(GameOverItem);

	// Reset Camera position
	let mut binding = set.p0();
	let mut camera_transform = binding.single_mut();
	camera_transform.translation.x = 0.0;
	camera_transform.translation.y = 0.0;

	points.points = 0;

	// Clear all entities from previous game
	let entity = set.p1().single();
	commands.entity(entity).despawn_recursive();

	for entity in &set.p2() {
		commands.entity(entity).despawn_recursive();
	}
	for entity in &set.p3() {
		commands.entity(entity).despawn_recursive();
	}
	for entity in &set.p4() {
		commands.entity(entity).despawn_recursive();
	}
}

fn update_game_over(
	keys: Res<Input<KeyCode>>,
	mut commands: Commands
) {
	if keys.just_pressed(KeyCode::Space) {
		commands.insert_resource(NextState(GameState::MainMenu));
	}
}

fn open_menu(
	keys: Res<Input<KeyCode>>,
	asset_server: Res<AssetServer>,
	mut commands: Commands,
	discovered_spells: Res<DiscoveredSpells>,
	query_player: Query<&Player>
) {
	if keys.just_pressed(KeyCode::Tab) {
		let player = query_player.single();

		commands.insert_resource(NextState(GameState::Menu));

		// Background
		commands.spawn(NodeBundle {
			style: Style {
				size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
				justify_content: JustifyContent::Center,
				align_items: AlignItems::Center,
				..default()
			},
			background_color: Color::rgba(0.0, 0.0, 0.0, 0.3).into(),
			..default()
		})
		.with_children(|parent| {
			// Border
			parent.spawn(NodeBundle {
				style: Style {
					size: Size::new(Val::Px(1000.0), Val::Px(600.0)),
					border: UiRect::all(Val::Px(2.0)),
					justify_content: JustifyContent::Center,
					align_items: AlignItems::Center,
					..default()
				},
				background_color: COLOR_FOREGROUND.into(),
				..default()
			})
			.with_children(|parent| {
				// Background
				parent.spawn(NodeBundle {
					style: Style {
						size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
						justify_content: JustifyContent::SpaceBetween,
						..default()
					},
					background_color: COLOR_BACKGROUND.into(),
					..default()
				})
				.with_children(|parent| {
					// Crafting Area (left)
					parent.spawn(NodeBundle {
						style: Style {
							size: Size::new(Val::Percent(50.0), Val::Percent(100.0)),
							justify_content: JustifyContent::SpaceBetween,
							align_content: AlignContent::Center,
							flex_direction: FlexDirection::ColumnReverse,
							..default()
						},
						background_color: Color::NONE.into(),
						..default()
					})
					.with_children(|parent| {
						create_crafting_area(&asset_server, parent, &discovered_spells, player);
					});

					// Seperator
					parent.spawn(NodeBundle {
						style: Style {
							size: Size::new(Val::Px(2.0), Val::Percent(100.0)),
							..default()
						},
						background_color: COLOR_FOREGROUND.into(),
						..default()
					});

					// Spell List (right)
					parent.spawn(NodeBundle {
						style: Style {
							size: Size::new(Val::Percent(50.0), Val::Percent(100.0)),
							justify_content: JustifyContent::SpaceBetween,
							align_content: AlignContent::Center,
							flex_direction: FlexDirection::ColumnReverse,
							..default()
						},
						background_color: Color::NONE.into(),
						..default()
					})
					.with_children(|parent| {
						create_spell_area(&asset_server, parent, &discovered_spells, &player);
					});
				});
			});
		})
		.insert(MenuItem);
	}
}

fn close_menu(
	keys: Res<Input<KeyCode>>,
	query: Query<Entity, With<MenuItem>>,
	mut commands: Commands
) {
	if keys.just_pressed(KeyCode::Tab) {
		commands.insert_resource(NextState(GameState::Playing));

		for entity in &query {
			commands.entity(entity).despawn_recursive();
		}
	}
}

fn select_crafting_spell(
	query_interaction: Query<(&Parent, &Interaction), Changed<Interaction>>,
	query: Query<
		(&Parent, Option<&CraftingRow1>, Option<&CraftingRow2>, Option<&CraftingFire>, Option<&CraftingIce>, Option<&CraftingEnergy>),
		Or<(With<CraftingRow1>, With<CraftingRow2>)>>,
	query_background: Query<&Parent, (Without<CraftingRow1>, Without<CraftingRow2>)>,
	mut query_slot: Query<&mut BackgroundColor, With<SpellSlot>>,
	query_row1: Query<(&Parent, Option<&CraftingFire>, Option<&CraftingIce>, Option<&CraftingEnergy>), (With<CraftingRow1>, Without<CraftingRow2>)>,
	query_row2: Query<(&Parent, Option<&CraftingFire>, Option<&CraftingIce>, Option<&CraftingEnergy>), (With<CraftingRow2>, Without<CraftingRow1>)>,
	mut query_cost_text: Query<&mut Text, With<CraftingCostText>>,
	mut query_crafting_result: Query<(&mut UiImage, &mut SpellType), With<CraftingResult>>,
	points: Res<Points>,
	asset_server: Res<AssetServer>,
	discovered_spells: Res<DiscoveredSpells>,
) {
	for (parent, interaction) in &query_interaction {
		if *interaction != Interaction::Clicked {
			continue;
		}

		if let Ok((parent, row1, row2, fire, ice, energy)) = query.get(parent.get()) {
			let mut row1_selected = SpellType::None;
			let mut row2_selected = SpellType::None;

			// Deselect old item in the same row
			// and find types for row 1 and 2 to show new result
			if row1.is_some() {
				if fire.is_some() {
					row1_selected = SpellType::Fire;
				} else if ice.is_some() {
					row1_selected = SpellType::Ice;
				} else if energy.is_some() {
					row1_selected = SpellType::Energy;
				}

				for (parent, _, _, _) in &query_row1 {
					let background = query_background.get(parent.get()).unwrap();
					let mut color = query_slot.get_mut(background.get()).unwrap();
					*color = COLOR_FOREGROUND.into();
				}
				for (parent, fire, ice, energy) in &query_row2 {
					let background = query_background.get(parent.get()).unwrap();
					let color = query_slot.get(background.get()).unwrap();
					if color.0 == COLOR_SELECTED {
						if fire.is_some() {
							row2_selected = SpellType::Fire;
						} else if ice.is_some() {
							row2_selected = SpellType::Ice;
						} else if energy.is_some() {
							row2_selected = SpellType::Energy;
						}
					}
				}
			} else if row2.is_some() {
				if fire.is_some() {
					row2_selected = SpellType::Fire;
				} else if ice.is_some() {
					row2_selected = SpellType::Ice;
				} else if energy.is_some() {
					row2_selected = SpellType::Energy;
				}

				for (parent, _, _, _) in &query_row2 {
					let background = query_background.get(parent.get()).unwrap();
					let mut color = query_slot.get_mut(background.get()).unwrap();
					*color = COLOR_FOREGROUND.into();
				}
				for (parent, fire, ice, energy) in &query_row1 {
					let background = query_background.get(parent.get()).unwrap();
					let color = query_slot.get(background.get()).unwrap();
					if color.0 == COLOR_SELECTED {
						if fire.is_some() {
							row1_selected = SpellType::Fire;
						} else if ice.is_some() {
							row1_selected = SpellType::Ice;
						} else if energy.is_some() {
							row1_selected = SpellType::Energy;
						}
					}
				}
			}

			// Select selected item
			let background = query_background.get(parent.get()).unwrap();
			let mut color = query_slot.get_mut(background.get()).unwrap();

			*color = COLOR_SELECTED.into();

			// Update Crafting result
			let (mut result_image, mut crafting_result) = query_crafting_result.single_mut();

			*crafting_result = get_crafting_result(&row1_selected, &row2_selected);
			if *crafting_result == SpellType::None {
				*crafting_result = get_crafting_result(&row2_selected, &row1_selected);
			}

			let mut cost_text = query_cost_text.single_mut();

			if *crafting_result == SpellType::None {
				result_image.0 = asset_server.load("icons/empty.png");
				cost_text.sections[0].value = "".to_string();
				return;
			} else {
				result_image.0 = asset_server.load(get_spell_icon(&discovered_spells, &crafting_result));
				cost_text.sections[0].value = format!("Cost: {:?}\n(Current: {:?})", get_spell_cost(&crafting_result), points.points)
			}
		}
	}
}

fn craft_spell(
	query_interaction: Query<(&Parent, &Interaction), Changed<Interaction>>,
	mut query: Query<(&mut UiImage, &mut SpellType), With<CraftingResult>>,
	mut points: ResMut<Points>,
	mut discovered_spells: ResMut<DiscoveredSpells>,
	mut query_spell_list: Query<(&mut UiImage, &SpellType), (With<SpellList>, Without<CraftingResult>)>,
	mut query_cost_text: Query<&mut Text, With<CraftingCostText>>,
	asset_server: Res<AssetServer>
) {
	for (parent, interaction) in &query_interaction {
		if *interaction == Interaction::Clicked {
			if let Ok((mut result_image, mut spell_type)) = query.get_mut(parent.get()) {
				if !is_spell_discovered(&discovered_spells, &spell_type) && points.points >= get_spell_cost(&spell_type) {
					points.points -= get_spell_cost(&spell_type);

					match *spell_type {
						SpellType::Flamethrower => discovered_spells.flamethrower = true,
						SpellType::Bomb => discovered_spells.bomb = true,
						SpellType::Plasma => discovered_spells.plasma = true,
						SpellType::Snowball => discovered_spells.snowball = true,
						SpellType::Snowman => discovered_spells.snowman = true,
						SpellType::Lightning => discovered_spells.lightning = true,
						_ => {}
					}

					update_discovered_spells(&discovered_spells, &asset_server, &mut query_spell_list);

					// Reset Crafting result
					let mut cost_text = query_cost_text.single_mut();

					result_image.0 = asset_server.load("icons/empty.png");
					*spell_type = SpellType::None;
					cost_text.sections[0].value = "".to_string();
				}
			}
		}
	}
}

fn update_spell_desctiption(
	query_interaction: Query<(&Parent, &Interaction), Changed<Interaction>>,
	query: Query<&SpellType, With<SpellList>>,
	mut query_text: Query<&mut Text, With<SpellDescriptionText>>,
	discovered_spells: Res<DiscoveredSpells>
) {
	let mut text = query_text.single_mut();

	if !query_interaction.is_empty() {
		text.sections[0].value = "".to_string();
	}

	for (parent, interaction) in &query_interaction {
		if *interaction == Interaction::Hovered || *interaction == Interaction::Clicked {
			if let Ok(spell_type) = &query.get(parent.get()) {
				if is_spell_discovered(&discovered_spells, &spell_type) {
					text.sections[0].value = get_spell_desctiption(spell_type).to_string();
				}
			}
		}
	}
}

fn select_spell(
	query_interaction: Query<(&Parent, &Interaction)>,
	mut query_player: Query<&mut Player>,
	query: Query<&SpellType, With<SpellList>>,
	mouse: Res<Input<MouseButton>>,
	keys: Res<Input<KeyCode>>,
	mut query_selected_spell_icon: Query<(&mut UiImage, &Parent), With<SelectedSpellIcon>>,
	query_spell_type: Query<&SpellType>,
	asset_server: Res<AssetServer>,
	discovered_spells: Res<DiscoveredSpells>
) {
	let mut player = query_player.single_mut();

	for (parent, interaction) in &query_interaction {
		if *interaction == Interaction::Hovered || *interaction == Interaction::Clicked {
			if let Ok(spell_type) = query.get(parent.get()) {
				if !is_spell_discovered(&discovered_spells, &spell_type) {
					continue;
				}

				if mouse.pressed(MouseButton::Left) {
					player.active_spell_mouse_left = *spell_type;

					if player.active_spell_mouse_right == *spell_type {
						player.active_spell_mouse_right = SpellType::None;
					}
					if player.active_spell_key_e == *spell_type {
						player.active_spell_key_e = SpellType::None;
					}

					update_selected_spell_icons(&mut query_selected_spell_icon, &query_spell_type, &player, &asset_server);
				}

				if mouse.pressed(MouseButton::Right) {
					player.active_spell_mouse_right = *spell_type;

					if player.active_spell_mouse_left == *spell_type {
						player.active_spell_mouse_left = SpellType::None;
					}
					if player.active_spell_key_e == *spell_type {
						player.active_spell_key_e = SpellType::None;
					}

					update_selected_spell_icons(&mut query_selected_spell_icon, &query_spell_type, &player, &asset_server);
				}

				if keys.pressed(KeyCode::E) {
					player.active_spell_key_e = *spell_type;

					if player.active_spell_mouse_left == *spell_type {
						player.active_spell_mouse_left = SpellType::None;
					}
					if player.active_spell_mouse_right == *spell_type {
						player.active_spell_mouse_right = SpellType::None;
					}

					update_selected_spell_icons(&mut query_selected_spell_icon, &query_spell_type, &player, &asset_server);
				}
			}
		}
	}
}

///////////////////////////////////////////////////////////////////
//                       F U N C T I O N S                       //
///////////////////////////////////////////////////////////////////

fn create_crafting_area(
	asset_server: &AssetServer,
	parent: &mut ChildBuilder,
	discovered_spells: &DiscoveredSpells,
	player: &Player
) {
	// Title
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Px(250.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(TextBundle::from_section(
				"Craft new Spell",
				TextStyle {
					font: asset_server.load("FiraSans-Bold.ttf"),
					font_size: 32.0,
					color: COLOR_TEXT
				}
			)
			.with_text_alignment(TextAlignment::CENTER)
		);
	});

	// First Row
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::SpaceAround,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		create_spell(&asset_server, parent, SpellType::Fire, discovered_spells, player, CraftingFire, CraftingRow1);
		create_spell(&asset_server, parent, SpellType::Ice, discovered_spells, player, CraftingIce, CraftingRow1);
		create_spell(&asset_server, parent, SpellType::Energy, discovered_spells, player, CraftingEnergy, CraftingRow1);
	});

	// Plus Sign
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(TextBundle::from_section(
				"+",
				TextStyle {
					font: asset_server.load("FiraSans-Bold.ttf"),
					font_size: 50.0,
					color: COLOR_TEXT
				}
			)
			.with_text_alignment(TextAlignment::CENTER)
		);
	});

	// Second Row
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::SpaceAround,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		create_spell(&asset_server, parent, SpellType::Fire, discovered_spells, player, CraftingFire, CraftingRow2);
		create_spell(&asset_server, parent, SpellType::Ice, discovered_spells, player, CraftingIce, CraftingRow2);
		create_spell(&asset_server, parent, SpellType::Energy, discovered_spells, player, CraftingEnergy, CraftingRow2);
	});

	// Arrow
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(TextBundle::from_section(
				"V",
				TextStyle {
					font: asset_server.load("FiraSans-Bold.ttf"),
					font_size: 50.0,
					color: COLOR_TEXT
				}
			)
			.with_text_alignment(TextAlignment::CENTER)
		);
	});

	// Result
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		create_spell(&asset_server, parent, SpellType::None, discovered_spells, player, CraftingResult, SpellType::None);
	});

	// Cost
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(TextBundle::from_section(
				"",
				TextStyle {
					font: asset_server.load("FiraSans-Bold.ttf"),
					font_size: 32.0,
					color: COLOR_TEXT
				}
			)
			.with_text_alignment(TextAlignment::CENTER)
		)
		.insert(CraftingCostText);
	});
}

fn create_spell_area(
	asset_server: &AssetServer,
	parent: &mut ChildBuilder,
	discovered_spells: &DiscoveredSpells,
	player: &Player
) {
	// Title
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Px(170.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(TextBundle::from_section(
				"Available Spells",
				TextStyle {
					font: asset_server.load("FiraSans-Bold.ttf"),
					font_size: 32.0,
					color: COLOR_TEXT
				}
			)
			.with_text_alignment(TextAlignment::CENTER)
		);
	});

	// First Row
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::SpaceAround,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		create_spell(&asset_server, parent, SpellType::Fireball, discovered_spells, player, SpellType::Fireball, SpellList);
		create_spell(&asset_server, parent, SpellType::IceSpeer, discovered_spells, player, SpellType::IceSpeer, SpellList);
		create_spell(&asset_server, parent, SpellType::EnergyBolt, discovered_spells, player, SpellType::EnergyBolt, SpellList);
	});

	// Second Row
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::SpaceAround,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		create_spell(&asset_server, parent, SpellType::Flamethrower, discovered_spells, player, SpellType::Flamethrower, SpellList);
		create_spell(&asset_server, parent, SpellType::Bomb, discovered_spells, player, SpellType::Bomb, SpellList);
		create_spell(&asset_server, parent, SpellType::Plasma, discovered_spells, player, SpellType::Plasma, SpellList);
	});

	// Thrid Row
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::SpaceAround,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		create_spell(&asset_server, parent, SpellType::Snowball, discovered_spells, player, SpellType::Snowball, SpellList);
		create_spell(&asset_server, parent, SpellType::Snowman, discovered_spells, player, SpellType::Snowman, SpellList);
		create_spell(&asset_server, parent, SpellType::Lightning, discovered_spells, player, SpellType::Lightning, SpellList);
	});

	// Description
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Percent(100.0), Val::Px(500.0)),
			align_content: AlignContent::Center,
			justify_content: JustifyContent::Center,
			..default()
		},
		background_color: Color::NONE.into(),
		..default()
	})
	.with_children(|parent| {
		parent.spawn(TextBundle::from_section(
				"",
				TextStyle {
					font: asset_server.load("FiraSans-Bold.ttf"),
					font_size: 25.0,
					color: COLOR_TEXT
				}
			)
			.with_text_alignment(TextAlignment::CENTER)
		)
		.insert(SpellDescriptionText);
	});
}

fn create_spell<T: Component + std::marker::Copy, U: Component + std::marker::Copy>(
	asset_server: &AssetServer,
	parent: &mut ChildBuilder,
	spell_type: SpellType,
	discovered_spells: &DiscoveredSpells,
	player: &Player,
	component1: T,
	component2: U
) {
	parent.spawn(NodeBundle {
		style: Style {
			size: Size::new(Val::Px(64.0), Val::Px(64.0)),
			border: UiRect::all(Val::Px(2.0)),
			justify_content: JustifyContent::Center,
			align_items: AlignItems::Center,
			..default()
		},
		background_color: COLOR_FOREGROUND.into(),
		..default()
	})
	.insert(SpellSlot)
	.with_children(|parent| {
		parent.spawn(NodeBundle {
			style: Style {
				size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
				justify_content: JustifyContent::Center,
				align_items: AlignItems::Center,
				..default()
			},
			background_color: COLOR_BACKGROUND.into(),
			..default()
		})
		.with_children(|parent| {
			parent.spawn(ImageBundle {
				image: asset_server.load(get_spell_icon(discovered_spells, &spell_type)).into(),
				..default()
			})
			.insert(component1)
			.insert(component2)
			.with_children(|parent| {
					let mut path = "icons/empty.png";

					if spell_type != SpellType::None {
						if player.active_spell_mouse_left == spell_type {
							path = "icons/mouse_left.png"
						} else if player.active_spell_mouse_right == spell_type {
							path = "icons/mouse_right.png"
						} else if player.active_spell_key_e == spell_type {
							path = "icons/key_e.png"
						}
					}

					parent.spawn(ImageBundle {
						image: asset_server.load(path).into(),
						..default()
					})
					.insert(SelectedSpellIcon)
					.insert(Interaction::None);
			});
		});
	});
}

fn update_discovered_spells(
	discovered_spells: &DiscoveredSpells,
	asset_server: &AssetServer,
	query: &mut Query<(&mut UiImage, &SpellType), (With<SpellList>, Without<CraftingResult>)>
) {
	for (mut image, spell_type) in query {
		*image = asset_server.load(get_spell_icon(discovered_spells, &spell_type)).into();
	}
}

fn update_selected_spell_icons(
	query: &mut Query<(&mut UiImage, &Parent), With<SelectedSpellIcon>>,
	query_spell_type: &Query<&SpellType>,
	player: &Player,
	asset_server: &AssetServer
) {
	for (mut image, parent) in query {
		if let Ok(spell_type) = query_spell_type.get(parent.get()) {
			if *spell_type == SpellType::None {
				continue;
			}

			if *spell_type == player.active_spell_mouse_left {
				*image = asset_server.load("icons/mouse_left.png").into();
			} else if *spell_type == player.active_spell_mouse_right {
				*image = asset_server.load("icons/mouse_right.png").into();
			} else if *spell_type == player.active_spell_key_e {
				*image = asset_server.load("icons/key_e.png").into();
			} else {
				*image = asset_server.load("icons/empty.png").into();
			}
		}
	}
}

fn get_crafting_result(item1: &SpellType, item2: &SpellType) -> SpellType {
	if *item1 == SpellType::Fire && *item2 == SpellType::Fire {
		SpellType::Flamethrower
	} else if *item1 == SpellType::Fire && *item2 == SpellType::Ice {
		SpellType::Bomb
	} else if *item1 == SpellType::Fire && *item2 == SpellType::Energy {
		SpellType::Plasma

	} else if *item1 == SpellType::Ice && *item2 == SpellType::Ice {
		SpellType::Snowman
	} else if *item1 == SpellType::Ice && *item2 == SpellType::Energy {
		SpellType::Snowball

	} else if *item1 == SpellType::Energy && *item2 == SpellType::Energy {
		SpellType::Lightning

	} else {
		SpellType::None
	}
}

fn get_spell_icon<'a>(
	discovered_spells: &DiscoveredSpells,
	spell_type: &SpellType
) -> &'a str {
	match spell_type {
		SpellType::None => "icons/empty.png",
		SpellType::Fire => "icons/fire.png",
		SpellType::Ice => "icons/ice.png",
		SpellType::Energy => "icons/energy.png",

		SpellType::Fireball => "icons/fireball.png",
		SpellType::IceSpeer => "icons/ice_speer.png",
		SpellType::EnergyBolt => "icons/energy_bolt.png",

		SpellType::Flamethrower => if is_spell_discovered(discovered_spells, &SpellType::Flamethrower) { "icons/flamethrower.png" } else { "icons/flamethrower_inactive.png" },
		SpellType::Bomb => if is_spell_discovered(discovered_spells, &SpellType::Bomb) { "icons/bomb.png" } else { "icons/bomb_inactive.png" },
		SpellType::Plasma => if is_spell_discovered(discovered_spells, &SpellType::Plasma) { "icons/plasma.png" } else { "icons/plasma_inactive.png" },
		SpellType::Snowball => if is_spell_discovered(discovered_spells, &SpellType::Snowball) { "icons/snowball.png" } else { "icons/snowball_inactive.png" },
		SpellType::Snowman => if is_spell_discovered(discovered_spells, &SpellType::Snowman) { "icons/snowman.png" } else { "icons/snowman_inactive.png" },
		SpellType::Lightning => if is_spell_discovered(discovered_spells, &SpellType::Lightning) { "icons/lightning.png" } else {"icons/lightning_inactive.png" }
	}
}

fn get_spell_desctiption(spell_type: &SpellType) -> &str {
	match spell_type {
		SpellType::Fireball => "A simple Fireball.\nNormal damage and attack speed.",
		SpellType::IceSpeer => "Throw an Ice Speer.\nSlower than a fireball but sightly more damage.",
		SpellType::EnergyBolt => "Shoots an Energy Bolt.\nThe fastest of the basic spells but\ndeals the least damage.",
		SpellType::Flamethrower => "Throw flames as if you were a Flamethrower.\nHigh damage but short range.",
		SpellType::Bomb => "Place a Bomb that will detonate into\ndeadly ice shards after one second.",
		SpellType::Plasma => "Spawn a Plasma Ball that will shoot\nout lightning onto nearby enemies and\ndoes a lot of damage itself.",
		SpellType::Snowball => "Throw a large Snowball onto your enemies\nthat will also damage nearby enemies on hit.",
		SpellType::Snowman => "Create a Decoy that will distract\nthe enemies for a short time.",
		SpellType::Lightning => "Shoot out Lightning that can\njump from enemy to enemy.",
		_ => ""
	}
}

fn get_spell_cost(spell_type: &SpellType) -> i32 {
	match spell_type {
		SpellType::Flamethrower => 400,
		SpellType::Bomb => 250,
		SpellType::Plasma => 250,
		SpellType::Snowball => 350,
		SpellType::Snowman => 400,
		SpellType::Lightning => 400,
		_ => 0
	}
}

fn is_spell_discovered(
	discovered_spells: &DiscoveredSpells,
	spell_type: &SpellType
) -> bool {
	match spell_type {
		SpellType::Fireball => true,
		SpellType::IceSpeer => true,
		SpellType::EnergyBolt => true,
		SpellType::Flamethrower => discovered_spells.flamethrower,
		SpellType::Bomb => discovered_spells.bomb,
		SpellType::Plasma => discovered_spells.plasma,
		SpellType::Snowball => discovered_spells.snowball,
		SpellType::Snowman => discovered_spells.snowman,
		SpellType::Lightning => discovered_spells.lightning,
		_ => false
	}
}
