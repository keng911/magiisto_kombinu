#![windows_subsystem = "windows"]
use bevy::prelude::*;

use iyes_loopless::prelude::*;

mod common;
mod player;
mod spells;
mod enemy;
mod gui;
mod menu;

use common::{
	WINDOW_WIDTH,
	WINDOW_HEIGHT,
	GameState,
	DamagePlayerEvent,
	DamageEnemyEvent,
	DamageSnowmanEvent,
	Points,
	setup_map,
	check_collisions,
	animate_movement
};

use player::PlayerPlugin;
use spells::SpellPlugin;
use enemy::EnemyPlugin;
use gui::GuiPlugin;
use menu::MenuPlugin;

///////////////////////////////////////////////////////////////////
//                            M A I N                            //
///////////////////////////////////////////////////////////////////

fn main() {
	App::new()
		.insert_resource(Msaa { samples: 4 })
		.insert_resource(ClearColor(Color::rgb(0.13, 0.55, 0.11)))
		.add_plugins(DefaultPlugins.set(WindowPlugin {
			window: WindowDescriptor {
				width: WINDOW_WIDTH,
				height: WINDOW_HEIGHT,
				title: "Magiisto, kombinu!".to_string(),
				// vsync: true,
				resizable: false,
				..default()
			},
			..default()
		}))

		.add_loopless_state(GameState::MainMenu)
		.insert_resource::<Points>(Points {
			total_points: 0,
			points: 0
		})

		// Startup
		.add_startup_system(setup_map)

		// Events
		.add_event::<DamagePlayerEvent>()
		.add_event::<DamageEnemyEvent>()
		.add_event::<DamageSnowmanEvent>()

		// Main Plugins
		.add_plugin(PlayerPlugin)
		.add_plugin(EnemyPlugin)
		.add_plugin(SpellPlugin)
		.add_plugin(GuiPlugin)
		.add_plugin(MenuPlugin)

		// common systems
		.add_system(animate_movement.run_in_state(GameState::Playing))
		.add_system(check_collisions.run_in_state(GameState::Playing))

		.run();
}
