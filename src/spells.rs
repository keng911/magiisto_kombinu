use std::fmt;

use bevy::{
	prelude::*,
	sprite::Anchor,
	math::Vec3Swizzles,
	utils::Duration
};

use iyes_loopless::prelude::*;

use rand::prelude::*;

use crate::common::{
	GameState,
	Health,
	DamageEnemyEvent,
	DamageSnowmanEvent,
	Collision,
	Dead,
	get_world_pos_from_mouse_pos
};

use crate::player::Player;

use crate::gui::spawn_health_bar;

use crate::enemy::Enemy;

///////////////////////////////////////////////////////////////////
//                          C O N S T S                          //
///////////////////////////////////////////////////////////////////

const PROJECTILE_SPEED: f32 = 300.0;
const PROJECTILE_SPEED_PLASMA: f32 = 200.0;

const MAX_LIGHTNING_DIST: f32 = 128.0;
const MAX_PLASMA_LIGHTNING_DIST: f32 = 100.0;
const MAX_LIGHTNING_JUMPS: i32 = 4;
const MAX_PLASMA_LIGHTNING_JUMPS: i32 = 3;

impl fmt::Display for SpellType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

// Source of a lightning bolt: lightning spell or plasma ball
// Affects max distance and max jumps
#[derive(Copy, Clone, Reflect, Default, Debug)]
enum LightningType {
	#[default]
	Lightning,
	Plasma
}

///////////////////////////////////////////////////////////////////
//                          P L U G I N                          //
///////////////////////////////////////////////////////////////////

pub struct SpellPlugin;

impl Plugin for SpellPlugin {
	fn build(&self, app: &mut App) {
		app
			.init_resource::<AttackCooldowns>()
			.add_system(player_attack.run_in_state(GameState::Playing))
			.add_system(move_projectiles.run_in_state(GameState::Playing))
			.add_system(despawn_spells.run_in_state(GameState::Playing))
			.add_system(check_projectile_collision.run_in_state(GameState::Playing))
			.add_system(create_lightnings.run_in_state(GameState::Playing))
			.add_system(create_plasma_lightnings.run_in_state(GameState::Playing))
			.add_system(snowman_receive_damage.run_in_state(GameState::Playing));
	}
}

///////////////////////////////////////////////////////////////////
//                         S T R U C T S                         //
///////////////////////////////////////////////////////////////////

#[derive(Clone, Copy, Reflect, Default, Debug, PartialEq, Component)]
pub enum SpellType {
	// Elements
	// These are not spells, they're only used to craft spells
	None,
	Fire,
	Ice,
	Energy,
	// Basic
	#[default]
	Fireball,
	IceSpeer,
	EnergyBolt,
	// Combined
	Flamethrower, // Fire + Fire
	Bomb,         // Fire + Ice
	Plasma,       // Fire + Energy
	Snowball,     // Ice + Energy
	Snowman,      // Ice + Ice
	Lightning     // Energy + Energy
}

#[derive(Component)]
pub struct Spell {
	damage: f32,
	lifetime: Timer
}

#[derive(Component)]
pub struct Projectile {
	speed: f32
}
#[derive(Resource)]
struct AttackCooldowns {
	fireball: Timer,
	ice_speer: Timer,
	energy_bolt: Timer,
	flamethrower: Timer,
	bomb: Timer,
	plasma: Timer,
	snowball: Timer,
	snowman: Timer,
	lightning: Timer
}

impl FromWorld for AttackCooldowns {
	fn from_world(_world: &mut World) -> Self {
		AttackCooldowns {
			fireball: Timer::new(Duration::from_millis(500), TimerMode::Once),
			ice_speer: Timer::new(Duration::from_millis(700), TimerMode::Once),
			energy_bolt: Timer::new(Duration::from_millis(400), TimerMode::Once),
			flamethrower: Timer::new(Duration::from_millis(100), TimerMode::Once),
			bomb: Timer::new(Duration::from_millis(1200), TimerMode::Once),
			plasma: Timer::new(Duration::from_millis(1100), TimerMode::Once),
			snowball: Timer::new(Duration::from_millis(1100), TimerMode::Once),
			snowman: Timer::new(Duration::from_millis(8000), TimerMode::Once),
			lightning: Timer::new(Duration::from_millis(100), TimerMode::Once)
		}
	}
}

#[derive(Component)]
pub struct SpellFireball;

#[derive(Component)]
pub struct SpellIceSpeer;

#[derive(Component)]
pub struct SpellEnergyBolt;

#[derive(Component)]
pub struct SpellFlamethrower;

#[derive(Component)]
pub struct SpellBomb;

#[derive(Component)]
pub struct SpellPlasma {
	lightning_timer: Timer
}

#[derive(Component)]
pub struct SpellSnowball;

#[derive(Component)]
pub struct SpellSnowman;

#[derive(Component)]
pub struct SpellLightning {
	target: Vec2,
	jumps: i32,
	did_damage: bool,
	chain_did_damage_to: Vec<u64>,
	lightning_type: LightningType
}

///////////////////////////////////////////////////////////////////
//                         S Y S T E M S                         //
///////////////////////////////////////////////////////////////////

fn player_attack(
	mut cooldowns: ResMut<AttackCooldowns>,
	time: Res<Time>,
	mouse: Res<Input<MouseButton>>,
	keys: Res<Input<KeyCode>>,
	query: Query<(&Transform, &Player)>,
	mut commands: Commands,
	windows: Res<Windows>,
	query_camera: Query<(&Camera, &GlobalTransform)>,
	asset_server: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
	snowman_query: Query<Entity, With<SpellSnowman>>
) {
	cooldowns.fireball.tick(time.delta());
	cooldowns.ice_speer.tick(time.delta());
	cooldowns.energy_bolt.tick(time.delta());
	cooldowns.flamethrower.tick(time.delta());
	cooldowns.bomb.tick(time.delta());
	cooldowns.plasma.tick(time.delta());
	cooldowns.snowball.tick(time.delta());
	cooldowns.snowman.tick(time.delta());
	cooldowns.lightning.tick(time.delta());

	let (player_transform, player) = query.single();

	if mouse.pressed(MouseButton::Left) {
		spawn_spell(
			player_transform,
			&query_camera,
			&windows,
			player.active_spell_mouse_left,
			&mut cooldowns,
			&mut commands,
			&asset_server,
			&snowman_query,
			&mut texture_atlases,
		);
	}

	if mouse.pressed(MouseButton::Right) {
		spawn_spell(
			player_transform,
			&query_camera,
			&windows,
			player.active_spell_mouse_right,
			&mut cooldowns,
			&mut commands,
			&asset_server,
			&snowman_query,
			&mut texture_atlases,
		);
	}

	if keys.pressed(KeyCode::E) {
		spawn_spell(
			player_transform,
			&query_camera,
			&windows,
			player.active_spell_key_e,
			&mut cooldowns,
			&mut commands,
			&asset_server,
			&snowman_query,
			&mut texture_atlases,
		);
	}
}

fn move_projectiles(
	mut query: Query<(&mut Transform, &Projectile)>,
	time: Res<Time>
) {
	for (mut transform, projectile) in &mut query {
		let movement_direction = transform.rotation * Vec3::Y;
		let movement_distance = projectile.speed * time.delta_seconds();
		let translation_delta = movement_direction * movement_distance;

		transform.translation += translation_delta;
	}
}

fn despawn_spells(
	mut commands: Commands,
	mut query: Query<(Entity, &mut Spell, &Transform, Option<&SpellBomb>, Option<&SpellSnowball>)>,
	asset_server: Res<AssetServer>,
	time: Res<Time>
) {
	for (entity, mut spell, transform, bomb, snowball) in &mut query {
		spell.lifetime.tick(time.delta());

		if spell.lifetime.finished() {
			if bomb.is_some() {
				detonate_bomb(&transform.translation, &mut commands, &asset_server);
			} else if snowball.is_some() {
				detonate_snowball(&transform.translation, &mut commands, &asset_server);
			}

			commands.entity(entity).despawn_recursive();
		}
	}
}

fn check_projectile_collision(
	mut commands: Commands,
	projectile_query: Query<(Entity, &Spell, &Transform, Option<&SpellSnowball>), With<Projectile>>,
	enemy_query: Query<(Entity, &Transform), (With<Enemy>, Without<Dead>, Without<Projectile>)>,
	asset_server: Res<AssetServer>,
	mut event_writer: EventWriter<DamageEnemyEvent>
) {
	for (entity, spell, transform, snowball) in &projectile_query {
		for (enemy, enemy_transform) in &enemy_query {
			if transform.translation.distance(enemy_transform.translation) < 40.0 {
				if snowball.is_some() {
					detonate_snowball(&transform.translation, &mut commands, &asset_server);
				}

				commands.entity(entity).despawn_recursive();

				event_writer.send(DamageEnemyEvent {
					entity: enemy,
					damage: spell.damage
				});
			}
		}
	}
}

fn create_lightnings(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>,
	mut query: Query<(&mut SpellLightning, &Spell)>,
	enemy_query: Query<(Entity, &Transform), (With<Enemy>, Without<Dead>)>,
	mut event_writer: EventWriter<DamageEnemyEvent>
) {
	// look for the nearest enemy. if it is close enough (less than max_dist), create a lightning
	for (mut lightning, spell) in &mut query {
		let max_dist = match lightning.lightning_type {
			LightningType::Lightning => MAX_LIGHTNING_DIST,
			LightningType::Plasma => MAX_PLASMA_LIGHTNING_DIST
		};
		let max_jumps = match lightning.lightning_type {
			LightningType::Lightning => MAX_LIGHTNING_JUMPS,
			LightningType::Plasma => MAX_PLASMA_LIGHTNING_JUMPS
		};

		if lightning.jumps > max_jumps || lightning.did_damage {
			return;
		}

		create_child_lightnings(
			&mut lightning,
			&enemy_query,
			&mut event_writer,
			max_dist,
			spell.damage,
			&mut commands,
			&asset_server,
			&mut texture_atlases
		);
	}
}

fn create_plasma_lightnings(
	time: Res<Time>,
	mut commands: Commands,
	mut query: Query<(&Transform, &mut SpellPlasma)>,
	enemy_query: Query<(Entity, &Transform), (With<Enemy>, Without<Dead>)>,
	mut event_writer: EventWriter<DamageEnemyEvent>,
	asset_server: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>
) {
	for (transform, mut plasma) in &mut query {
		plasma.lightning_timer.tick(time.delta());

		if plasma.lightning_timer.just_finished() {
			// Dummy lightning to spawn children from
			let mut lightning = SpellLightning {
				target: transform.translation.xy(),
				jumps: -1,
				did_damage: false,
				chain_did_damage_to: Vec::new(),
				lightning_type: LightningType::Plasma
			};

			create_child_lightnings(
				&mut lightning,
				&enemy_query,
				&mut event_writer,
				MAX_PLASMA_LIGHTNING_DIST,
				6.0,
				&mut commands,
				&asset_server,
				&mut texture_atlases
			);
		}
	}
}

fn snowman_receive_damage(
	mut commands: Commands,
	mut events: EventReader<DamageSnowmanEvent>,
	mut query: Query<(Entity, &mut Health), With<SpellSnowman>>
) {
	for (entity, mut health) in &mut query {
		for event in events.iter() {
			health.health = health.health - event.damage;

			if health.health <= 0.0 {
				commands.entity(entity).despawn_recursive();
			}
		}
	}
}

///////////////////////////////////////////////////////////////////
//                       F U N C T I O N S                       //
///////////////////////////////////////////////////////////////////

fn spawn_spell(
	player_transform: &Transform,
	query_camera: &Query<(&Camera, &GlobalTransform)>,
	windows: &Windows,
	spell_type: SpellType,
	cooldowns: &mut AttackCooldowns,
	commands: &mut Commands,
	asset_server: &AssetServer,
	snowman_query: &Query<Entity, With<SpellSnowman>>,
	texture_atlases: &mut Assets<TextureAtlas>,
) {
	let (camera, camera_transform) = query_camera.single();
	if let Some(mouse_position) = get_world_pos_from_mouse_pos(&windows, &camera, &camera_transform) {

		match spell_type {
			SpellType::Fireball => {
				if cooldowns.fireball.finished() {
					cooldowns.fireball.reset();
					spawn_fireball(&player_transform, commands, &asset_server);
				}
			},
			SpellType::IceSpeer => {
				if cooldowns.ice_speer.finished() {
					cooldowns.ice_speer.reset();
					spawn_ice_speer(&player_transform, commands, &asset_server);
				}
			},
			SpellType::EnergyBolt => {
				if cooldowns.energy_bolt.finished() {
					cooldowns.energy_bolt.reset();
					spawn_energy_bolt(&player_transform, commands, &asset_server);
				}
			},
			SpellType::Flamethrower => {
				if cooldowns.flamethrower.finished() {
					cooldowns.flamethrower.reset();
					spawn_flamethrower(&player_transform, commands, &asset_server);
				}
			},
			SpellType::Bomb => {
				if cooldowns.bomb.finished() {
					cooldowns.bomb.reset();
					spawn_bomb(&player_transform, commands, &asset_server);
				}
			},
			SpellType::Plasma => {
				if cooldowns.plasma.finished() {
					cooldowns.plasma.reset();
					spawn_plasma(&player_transform, commands, &asset_server);
				}
			},
			SpellType::Snowball => {
				if cooldowns.snowball.finished() {
					cooldowns.snowball.reset();
					spawn_snowball(&player_transform, commands, &asset_server);
				}
			},
			SpellType::Snowman => {
				if cooldowns.snowman.finished() {
					cooldowns.snowman.reset();
					spawn_snowman(&player_transform, commands, &asset_server, snowman_query);
				}
			},
			SpellType::Lightning => {
				if cooldowns.lightning.finished() {
					cooldowns.lightning.reset();
					spawn_lightning(&player_transform, mouse_position, commands, &asset_server, texture_atlases);
				}
			},
			_ => {}
		}
	}
}

fn spawn_fireball(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/fireball.png"),
		transform: *player_transform,
		..default()
	})
	.insert(Spell {
		damage: 10.0,
		lifetime: Timer::new(Duration::from_secs(5), TimerMode::Once)
	})
	.insert(Projectile {
		speed: PROJECTILE_SPEED
	})
	.insert(SpellFireball);
}

fn spawn_ice_speer(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/ice_speer.png"),
		transform: *player_transform,
		..default()
	})
	.insert(Spell {
		damage: 14.0,
		lifetime: Timer::new(Duration::from_secs(5), TimerMode::Once)
	})
	.insert(Projectile {
		speed: PROJECTILE_SPEED
	})
	.insert(SpellIceSpeer);
}

fn spawn_energy_bolt(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/energy_bolt.png"),
		transform: *player_transform,
		..default()
	})
	.insert(Spell {
		damage: 8.0,
		lifetime: Timer::new(Duration::from_secs(5), TimerMode::Once)
	})
	.insert(Projectile {
		speed: PROJECTILE_SPEED
	})
	.insert(SpellEnergyBolt);
}

fn spawn_flamethrower(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	let mut rng = thread_rng();

	for _ in 0..5 {
		let mut transform = player_transform.clone();
		// 1.2 rad = ~68 deg
		transform.rotate_z(rng.gen_range(-0.6..0.6));

		commands.spawn(SpriteBundle {
			texture: asset_server.load("spells/flame.png"),
			transform,
			..default()
		})
		.insert(Spell {
			damage: 2.0,
			lifetime: Timer::new(Duration::from_millis(600), TimerMode::Once)
		})
		.insert(Projectile {
			speed: PROJECTILE_SPEED
		})
		.insert(SpellFlamethrower);
	}
}

fn spawn_bomb(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/bomb.png"),
		transform: *player_transform,
		..default()
	})
	.insert(Spell {
		damage: 0.0,
		lifetime: Timer::new(Duration::from_secs(1), TimerMode::Once)
	})
	.insert(SpellBomb);
}

fn spawn_plasma(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/plasma.png"),
		transform: *player_transform,
		..default()
	})
	.insert(Spell {
		damage: 20.0,
		lifetime: Timer::new(Duration::from_secs(5), TimerMode::Once)
	})
	.insert(Projectile {
		speed: PROJECTILE_SPEED_PLASMA
	})
	.insert(SpellPlasma {
		lightning_timer: Timer::new(Duration::from_millis(150), TimerMode::Repeating)
	});
}

fn spawn_snowball(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer
) {
	commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/snowball.png"),
		transform: *player_transform,
		..default()
	})
	.insert(Spell {
		damage: 50.0,
		lifetime: Timer::new(Duration::from_secs(5), TimerMode::Repeating)
	})
	.insert(Projectile {
		speed: PROJECTILE_SPEED
	})
	.insert(SpellSnowball);
}

fn spawn_snowman(
	player_transform: &Transform,
	commands: &mut Commands,
	asset_server: &AssetServer,
	query: &Query<Entity, With<SpellSnowman>>
) {
	// Kill old snowman
	// There can only be one
	if !query.is_empty() {
		let entity = query.single();
		commands.entity(entity).despawn_recursive();
	}

	let mut rng = thread_rng();

	let direction = player_transform.rotation * Vec3::Y;
	let delta = direction * 100.0;
	let target = player_transform.translation + delta;

	// 6.28 = ~360° in radians
	let rotation = rng.gen_range(0.0..6.28);

	let entity = commands.spawn(SpriteBundle {
		texture: asset_server.load("spells/snowman.png"),
		transform: Transform::from_translation(target).with_rotation(Quat::from_rotation_z(rotation)),
		..default()
	})
	.insert(Spell {
		damage: 0.0,
		lifetime: Timer::new(Duration::from_secs(10), TimerMode::Once)
	})
	.insert(Health {
		max_health: 50.0,
		health: 50.0
	})
	.insert(SpellSnowman)
	.insert(Collision)
	.id();

	spawn_health_bar(commands, entity.to_bits());
}

fn spawn_lightning(
	player_transform: &Transform,
	mouse_position: Vec2,
	commands: &mut Commands,
	asset_server: &AssetServer,
	texture_atlases: &mut Assets<TextureAtlas>
) {
	let distance = player_transform.translation.xy().distance(mouse_position).min(300.0);

	// Calculate target pos
	let direction = player_transform.rotation * Vec3::Y;
	let delta = direction * distance;
	let target = player_transform.translation + delta;

	create_lightning(
		player_transform.translation.xy(),
		target.xy(),
		0,
		LightningType::Lightning,
		Vec::new(),
		6.0,
		commands,
		&asset_server,
		texture_atlases
	);
}

fn create_lightning(
	origin: Vec2,
	target: Vec2,
	jumps: i32,
	lightning_type: LightningType,
	chain_did_damage_to: Vec<u64>,
	damage: f32,
	commands: &mut Commands,
	asset_server: &AssetServer,
	texture_atlases: &mut Assets<TextureAtlas>
) {
	let mut rng = thread_rng();

	let texture_handle = asset_server.load("spells/lightning.png");
	let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(64.0, 128.0), 4, 1, None,None);
	let texture_atlas_handle = texture_atlases.add(texture_atlas);

	let rotation = Quat::from_rotation_arc(Vec3::Y, (target - origin).normalize().extend(0.0));

	commands.spawn(SpriteSheetBundle {
		texture_atlas: texture_atlas_handle,
		sprite: TextureAtlasSprite {
			index: rng.gen_range(0..4),
			anchor: Anchor::BottomCenter,
			custom_size: Some(Vec2::new(64.0, origin.distance(target))),
			..default()
		},
		transform: Transform::from_translation(origin.extend(1.0)).with_rotation(rotation),
		..default()
	})
	.insert(Spell {
		damage,
		lifetime: Timer::new(Duration::from_millis(100), TimerMode::Once)
	})
	.insert(SpellLightning {
		target,
		jumps,
		did_damage: false,
		chain_did_damage_to,
		lightning_type
	});
}

fn create_child_lightnings(
	lightning: &mut SpellLightning,
	enemy_query: &Query<(Entity, &Transform), (With<Enemy>, Without<Dead>)>,
	event_writer: &mut EventWriter<DamageEnemyEvent>,
	max_dist: f32,
	damage: f32,
	commands: &mut Commands,
	asset_server: &Res<AssetServer>,
	texture_atlases: &mut ResMut<Assets<TextureAtlas>>
) {
	for (enemy, enemy_transform) in enemy_query {
		if lightning.chain_did_damage_to.contains(&enemy.to_bits()) {
			continue;
		}

		let dist = lightning.target.distance(enemy_transform.translation.xy());
		if dist <= max_dist {
			// Save that this enemy recived damage from this lightning chain
			// so that no children damage it again
			lightning.chain_did_damage_to.push(enemy.to_bits());
			// Save that this lightning did already do damage
			lightning.did_damage = true;

			event_writer.send(DamageEnemyEvent {
				entity: enemy,
				damage
			});

			create_lightning(
				lightning.target,
				enemy_transform.translation.xy(),
				lightning.jumps + 1,
				lightning.lightning_type,
				lightning.chain_did_damage_to.clone(),
				damage * 0.8,
				commands,
				&asset_server,
				texture_atlases
			);

			return;
		}
	}
}

fn detonate_bomb(
	origin: &Vec3,
	commands: &mut Commands,
	asset_server: &Res<AssetServer>
) {
	let mut rng = thread_rng();

	for _ in 0..30 {
		// 6.28 = ~360° in radians
		let rotation = rng.gen_range(0.0..6.28);

		commands.spawn(SpriteBundle {
			texture: asset_server.load("spells/bomb_fragment.png"),
			transform: Transform::from_translation(*origin).with_rotation(Quat::from_rotation_z(rotation)),
			..default()
		})
		.insert(Spell {
			damage: 7.0,
			lifetime: Timer::new(Duration::from_millis(600), TimerMode::Once)
		})
		.insert(Projectile {
			speed: PROJECTILE_SPEED
		});
	}
}

fn detonate_snowball(
	origin: &Vec3,
	commands: &mut Commands,
	asset_server: &Res<AssetServer>
) {
	let mut rng = thread_rng();

	for _ in 0..20 {
		// 6.28 = ~360° in radians
		let rotation = rng.gen_range(0.0..6.28);

		let transform = Transform::from_translation(*origin).with_rotation(Quat::from_rotation_z(rotation));
		let direction = transform.rotation * Vec3::Y;
		let delta = direction * 60.0;
		let target = transform.translation + delta;

		commands.spawn(SpriteBundle {
			texture: asset_server.load("spells/snowball_fragment.png"),
			transform: Transform::from_translation(target).with_rotation(Quat::from_rotation_z(rotation)),
			..default()
		})
		.insert(Spell {
			damage: 3.0,
			lifetime: Timer::new(Duration::from_millis(400), TimerMode::Once)
		})
		.insert(Projectile {
			speed: PROJECTILE_SPEED
		});
	}
}
