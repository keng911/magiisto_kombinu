use bevy::{
	prelude::*,
	math::Vec3Swizzles,
	utils::Duration
};

use iyes_loopless::prelude::*;

use crate::common::{
	ROTATION_SPEED,
	GameState,
	Health,
	DamagePlayerEvent,
	Collision,
	MovementAnimation,
	get_world_pos_from_mouse_pos
};

use crate::spells::SpellType;

use crate::gui::spawn_health_bar;

///////////////////////////////////////////////////////////////////
//                          C O N S T S                          //
///////////////////////////////////////////////////////////////////

const MOVEMENT_SPEED: f32 = 200.0;

pub const CAMERA_MAX_DISTANCE_X: f32 = 240.0;
pub const CAMERA_MAX_DISTANCE_Y: f32 = 160.0;

///////////////////////////////////////////////////////////////////
//                          P L U G I N                          //
///////////////////////////////////////////////////////////////////

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
	fn build(&self, app: &mut App) {
		app
			.init_resource::<HealthRegenerationTimer>()
			.add_enter_system(GameState::Playing, setup_player)
			.add_system(player_regenerate_health.run_in_state(GameState::Playing))
			.add_system(player_movement.run_in_state(GameState::Playing).label("player_movement"))
			.add_system(player_rotation.run_in_state(GameState::Playing))
			.add_system(player_receive_damage.run_in_state(GameState::Playing));
	}
}

///////////////////////////////////////////////////////////////////
//                         S T R U C T S                         //
///////////////////////////////////////////////////////////////////

#[derive(Component)]
pub struct Player {
	pub active_spell_mouse_left: SpellType,
	pub active_spell_mouse_right: SpellType,
	pub active_spell_key_e: SpellType
}
#[derive(Resource)]
struct HealthRegenerationTimer {
	timer: Timer
}

impl FromWorld for HealthRegenerationTimer {
	fn from_world(_world: &mut World) -> Self {
		HealthRegenerationTimer {
			timer: Timer::new(Duration::from_millis(1400), TimerMode::Repeating)
		}
	}
}

///////////////////////////////////////////////////////////////////
//                         S Y S T E M S                         //
///////////////////////////////////////////////////////////////////

fn setup_player(
	query: Query<With<Player>>,
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	mut texture_atlases: ResMut<Assets<TextureAtlas>>
) {
	if !query.is_empty() {
		// Player already exists
		return;
	}

	let texture_handle = asset_server.load("player.png");
	let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(64.0, 64.0), 9, 1, None, None);
	let texture_atlas_handle = texture_atlases.add(texture_atlas);

	let entity = commands.spawn(SpriteSheetBundle {
		texture_atlas: texture_atlas_handle,
		sprite: TextureAtlasSprite {
			index: 0,
			..default()
		},
		transform: Transform::from_translation(Vec3::new(0.0, 0.0, 1.0)),
		..default()
	})
	.insert(Player {
		active_spell_mouse_left: SpellType::Fireball,
		active_spell_mouse_right: SpellType::EnergyBolt,
		active_spell_key_e: SpellType::IceSpeer
	})
	.insert(MovementAnimation {
		moving: false,
		timer: Timer::new(Duration::from_millis(250), TimerMode::Repeating)
	})
	.insert(Health {
		max_health: 100.0,
		health: 100.0
	})
	.insert(Collision)
	.id();

	spawn_health_bar(&mut commands, entity.to_bits());
}

pub fn player_movement(
	time: Res<Time>,
	keys: Res<Input<KeyCode>>,
	mut query: Query<(&mut Transform, &mut MovementAnimation), With<Player>>,
	mut query_camera: Query<&mut Transform, (With<Camera>, Without<Player>)>
) {
	let (mut transform, mut animation) = query.single_mut();
	let mut camera_transform = query_camera.single_mut();
	let mut velocity = Vec2::new(0.0, 0.0);

	if keys.pressed(KeyCode::W) {
		velocity.y += 1.0;
	}
	if keys.pressed(KeyCode::A) {
		velocity.x -= 1.0;
	}
	if keys.pressed(KeyCode::S) {
		velocity.y -= 1.0;
	}
	if keys.pressed(KeyCode::D) {
		velocity.x += 1.0;
	}

	if velocity != Vec2::ZERO {
		animation.moving = true;
		velocity = velocity.normalize() * MOVEMENT_SPEED * time.delta_seconds();
		transform.translation.x += velocity.x;
		transform.translation.y += velocity.y;

		transform.translation = transform.translation.clamp(Vec3::new(-1000.0, -1000.0, 1.0), Vec3::new(1000.0, 1000.0, 1.0));

		// Move Camera
		if (transform.translation.x - camera_transform.translation.x).abs() > CAMERA_MAX_DISTANCE_X {
			camera_transform.translation.x += velocity.x;
		}
		if (transform.translation.y - camera_transform.translation.y).abs() > CAMERA_MAX_DISTANCE_Y {
			camera_transform.translation.y += velocity.y;
		}
	} else {
		animation.moving = false;
	}
}

// Adopted from https://github.com/bevyengine/bevy/blob/2fa7b3b6d0d5e5e7e48ae1c68affa4edc65cbe6a/examples/2d/rotation.rs#L193
fn player_rotation(
	mut query: Query<&mut Transform, With<Player>>,
	windows: Res<Windows>,
	query_camera: Query<(&Camera, &GlobalTransform)>
) {
	let (camera, camera_transform) = query_camera.single();
	if let Some(mouse_position) = get_world_pos_from_mouse_pos(&windows, &camera, &camera_transform) {
		let mut transform = query.single_mut();
		let forward = (transform.rotation * Vec3::Y).xy();
		let to_mouse = (mouse_position - transform.translation.xy()).normalize();
		let forward_dot_mouse = forward.dot(to_mouse);

		if (forward_dot_mouse - 1.0).abs() < f32::EPSILON {
			return;
		}

		let right = (transform.rotation * Vec3::X).xy();
		let right_dot_mouse = right.dot(to_mouse);
		let rotation_sign = -f32::copysign(1.0, right_dot_mouse);
		let max_angle = forward_dot_mouse.clamp(-1.0, 1.0).acos();
		let rotation_angle = rotation_sign * (ROTATION_SPEED.min(max_angle));

		transform.rotate_z(rotation_angle);
	}
}

fn player_receive_damage(
	mut commands: Commands,
	mut events: EventReader<DamagePlayerEvent>,
	mut query: Query<&mut Health, With<Player>>
) {
	let mut health = query.single_mut();

	for event in events.iter() {
		health.health = health.health - event.damage;

		if health.health <= 0.0 {
			commands.insert_resource(NextState(GameState::GameOver));
		}
	}
}

fn player_regenerate_health(
	mut query: Query<&mut Health, With<Player>>,
	mut timer: ResMut<HealthRegenerationTimer>,
	time: Res<Time>
) {
	timer.timer.tick(time.delta());

	if timer.timer.finished() {
		let mut health = query.single_mut();

		if health.health < health.max_health {
			health.health += 0.5;
			// In case there are floating point errors
			if health.health > health.max_health {
				health.health = health.max_health;
			}
		}
	}
}

///////////////////////////////////////////////////////////////////
//                       F U N C T I O N S                       //
///////////////////////////////////////////////////////////////////
