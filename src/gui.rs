use bevy::prelude::*;

use iyes_loopless::prelude::*;

use crate::common::{
	GameState,
	Health,
	Points,
	Dead
};

///////////////////////////////////////////////////////////////////
//                          C O N S T S                          //
///////////////////////////////////////////////////////////////////

const MAX_HEALTH_BAR_SIZE: f32 = 100.0;

///////////////////////////////////////////////////////////////////
//                          P L U G I N                          //
///////////////////////////////////////////////////////////////////

pub struct GuiPlugin;

impl Plugin for GuiPlugin {
	fn build(&self, app: &mut App) {
		app
			.add_enter_system(GameState::Playing, setup_gui)
			.add_system(update_points_text.run_in_state(GameState::Playing))
			.add_system(update_health_bars
				.run_in_state(GameState::Playing)
				.after("player_movement")
				.after("enemy_move_attack")
			);
	}
}

///////////////////////////////////////////////////////////////////
//                         S T R U C T S                         //
///////////////////////////////////////////////////////////////////

#[derive(Component)]
pub struct HealthBar {
	for_entity: u64
}

#[derive(Component)]
struct PointsText;

///////////////////////////////////////////////////////////////////
//                         S Y S T E M S                         //
///////////////////////////////////////////////////////////////////

fn setup_gui(
	query: Query<With<PointsText>>,
	mut commands: Commands,
	asset_server: Res<AssetServer>
) {
	if !query.is_empty() {
		return;
	}

	commands.spawn(
		TextBundle::from_section(
			"0",
			TextStyle {
				font: asset_server.load("FiraSans-Bold.ttf"),
				font_size: 32.0,
				color: Color::rgb(0.1, 0.1, 0.1)
			}
		)
		.with_text_alignment(TextAlignment::TOP_RIGHT)
		.with_style(Style {
			align_self: AlignSelf::FlexEnd,
			position_type: PositionType::Absolute,
			position: UiRect {
				top: Val::Px(5.0),
				right: Val::Px(15.0),
				..default()
			},
			..default()
		}),
	)
	.insert(PointsText);
}

fn update_points_text(
	mut query: Query<&mut Text, With<PointsText>>,
	points: Res<Points>
) {
	if points.is_changed() {
		let mut text = query.single_mut();

		text.sections[0].value = points.points.to_string();
	}
}

fn update_health_bars(
	mut commands: Commands,
	mut query_health_bar: Query<(Entity, &mut Sprite, &mut Transform, &mut Visibility, &HealthBar)>,
	query_entity: Query<(&Transform, &Health), (Without<Dead>, Without<HealthBar>)>
) {
	for (entity, mut sprite, mut transform, mut visibility, health_bar) in &mut query_health_bar {
		if let Ok((entity_transform, health)) = query_entity.get(Entity::from_bits(health_bar.for_entity)) {
			if health.health < health.max_health {
				visibility.is_visible = true;
				// Update position
				transform.translation.x = entity_transform.translation.x;
				transform.translation.y = entity_transform.translation.y + 60.0;

				// Update size
				let percentage = health.health / health.max_health;
				sprite.custom_size = Some(Vec2::new(MAX_HEALTH_BAR_SIZE * percentage, 8.0));
			} else {
				visibility.is_visible = false;
			}
		} else {
			// Entity for this health bar not found, delete it
			commands.entity(entity).despawn_recursive();
		}
	}
}

///////////////////////////////////////////////////////////////////
//                       F U N C T I O N S                       //
///////////////////////////////////////////////////////////////////

pub fn spawn_health_bar(
	commands: &mut Commands,
	entity: u64
) {
	commands.spawn(SpriteBundle {
		transform: Transform {
			translation: Vec3::new(0.0, 0.0, 5.0),
			..default()
		},
		sprite: Sprite {
			color: Color::rgb(0.95, 0.08, 0.0).into(),
			// Spawn invisible before true size is set in update system
			custom_size: Some(Vec2::new(0.0, 0.0)),
			..default()
		},
		..default()
	})
	.insert(HealthBar {
		for_entity: entity
	});
}
